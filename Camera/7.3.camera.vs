#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 aTexCoord;

out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
	// 3d公式 摄像头的移动视图是通过计算cube的位置变化模拟出来的！
	gl_Position = projection * view * model * vec4(aPos, 1.0f);
	// 纹理坐标
	TexCoord = vec2(aTexCoord.x, aTexCoord.y);
}